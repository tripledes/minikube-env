#!/usr/bin/env bash

set -uo pipefail

BASEDIR="$(dirname "$0")"
ASSETSDIR="${BASEDIR}/kubernetes/assets"
MINIKUBE_PROFILE=tfm
MINIKUBE_CPUS=4
MINIKUBE_MEMORY=8192
MINIKUBE_DISK=50000
MINIKUBE_K8S_VERSION=1.18.3

die() {                  
    printf "==> \e[31m[ERROR] ==> failed %s \e[0m\n" "${1:-'unknown'}"                                                            
    exit 1
}

run_minikube() {
    PROFILE=$(minikube profile list | grep -c ${MINIKUBE_PROFILE})

    if [ "${PROFILE}" -eq 0 ]; then
        minikube profile ${MINIKUBE_PROFILE}
        minikube config set cpus ${MINIKUBE_CPUS}
        minikube config set memory ${MINIKUBE_MEMORY}
        minikube config set disk-size ${MINIKUBE_DISK}
        minikube config set kubernetes-version ${MINIKUBE_K8S_VERSION}
    fi
    
    printf "\e[36m[INFO] ==> Starting minikube profile %s ...\e[0m\n" ${MINIKUBE_PROFILE}
    minikube -p ${MINIKUBE_PROFILE} start
}

clean_minikube() {
    minikube stop -p ${MINIKUBE_PROFILE} || true
    minikube delete -p ${MINIKUBE_PROFILE} || true
}

deploy_assets() {
    for asset in "${ASSETSDIR}"/*.yaml; do
        printf "\e[36m[INFO] ==> applying asset %s ...\e[0m\n" "${asset}"
        kubectl create -f "${asset}" > /dev/null || die "applying asset ${asset}"
        sleep 5s
    done
    printf "\e[32m[SUCCESS] ==> all assets have been deployed!\e[0m\n"
}

undeploy_assets() {
    for asset in $(find "${ASSETSDIR}"/*.yaml | sort -r); do
        printf "\e[36m[INFO] ==> deleting asset %s ...\e[0m\n" "${asset}"
        kubectl delete -f "${asset}" > /dev/null || true
        sleep 2s
    done
}

deploy_strimzi() {
    printf "\e[36m[INFO] ==> Creating strimzi namespaces ...\e[0m\n"
    kubectl create ns strimzi || true
    printf "\e[36m[INFO] ==> Deploying strimzi operator ...\e[0m\n"
    kubectl apply -f kubernetes/strimzi/ -n strimzi
    printf "\e[36m[INFO] ==> Creating Kafka namespaces ...\e[0m\n"
    kubectl create ns streams || true
    kubectl apply -f kubernetes/streams/strimzi-rolebindings.yaml -n streams
    printf "\e[36m[INFO] ==> Waiting for strimzi operator to be ready ...\e[0m\n"
    kubectl wait deployment/strimzi-cluster-operator --for=condition=Available --timeout=600s -n strimzi
}

undeploy_strimzi() {
    printf "\e[36m[INFO] ==> Deploying Strimzi operator ...\e[0m\n"
    kubectl delete -f kubernetes/strimzi/ -n strimzi
    kubectl delete ns strimzi
}

deploy_kafka() {
    printf "\e[36m[INFO] ==> Deploying Kafka cluster ...\e[0m\n"
    kubectl apply -f kubernetes/streams/kafka-cluster.yaml -n streams
    printf "\e[36m[INFO] ==> Waiting for Kafka cluster to be ready ...\e[0m\n"
    kubectl wait kafka/streams-cluster --for=condition=Ready --timeout=600s -n streams
    kubectl apply -f kubernetes/streams/kafka-measure-topic.yaml -n streams
    kubectl apply -f kubernetes/streams/kafka-sensor-topic.yaml -n streams
    kubectl apply -f kubernetes/streams/kafka-short-term-streams.yaml -n streams
    kubectl apply -f kubernetes/streams/kafka-long-term-streams.yaml -n streams
    kubectl apply -f kubernetes/streams/kafka-streams-user.yaml -n streams
    kubectl apply -f kubernetes/streams/kafka-gateway-user.yaml -n streams
    printf "\e[32m[SUCCESS] ==> Kafka is ready to be used!\e[0m\n"
}

undeploy_kafka() {
    printf "\e[36m[INFO] ==> Deleting Kafka namespaces ...\e[0m\n"
    kubectl delete ns streams
}

deploy_es_operator() {
    printf "\e[36m[INFO] ==> Deploying Elastic operator ...\e[0m\n"
    kubectl apply -f kubernetes/es-operator/
    printf "\e[36m[INFO] ==> Waiting for the operator to be ready ...\e[0m\n"
    while ! kubectl wait pods -l control-plane=elastic-operator -n elastic-system --for=condition=Ready > /dev/null 2>&1; do
        sleep 2s
    done
}

undeploy_es_operator() {
    printf "\e[36m[INFO] ==> Undeploying Elastic operator ...\e[0m\n"
    kubectl delete -f kubernetes/es-operator/ -n elastic-system
}

deploy_es() {
    printf "\e[36m[INFO] ==> Deploying ElasticSearch instance ...\e[0m\n"
    kubectl create ns elasticsearch
    kubectl apply -f kubernetes/elasticsearch/01-filerealm.yaml -n elasticsearch
    kubectl apply -f kubernetes/elasticsearch/02-es-instance.yaml -n elasticsearch
    printf "\e[36m[INFO] ==> Waiting for the ElasticSearch instance to be ready...\e[0m\n"
    while ! kubectl wait pod -n elasticsearch -l elasticsearch.k8s.elastic.co/cluster-name=meteo-lt-cluster --for=condition=Ready > /dev/null 2>&1; do
        sleep 2s
    done
    printf "\e[36m[INFO] ==> Exposing ES on a NodePort ...\e[0m\n"
    kubectl apply -f kubernetes/elasticsearch/03-nodeport.yaml -n elasticsearch
    printf "\e[32m[SUCCESS] ==> ElasticSearch is ready to be used!\e[0m\n"
}

undeploy_es() {
    printf "\e[36m[INFO] ==> Undeploying ElasticSearch ...\e[0m\n"
    kubectl delete ns elasticsearch
}

deploy_grafana() {
    printf "\e[36m[INFO] ==> Deploying and exposing grafana ...\e[0m\n"
    kubectl apply -f kubernetes/monitoring
}

undeploy_grafana() {
    printf "\e[36m[INFO] ==> Removing grafana ...\e[0m\n"
    kubectl delete -f kubernetes/monitoring
}

if [ $# -ne 1 ]; then
    die "Usage: $0 (all|undeploy|cide|uncicd|kafka|unkafka|es|unes|start_k8s|rm_k8s)"
fi

case $1 in
    "all")
        deploy_assets
        deploy_grafana
        deploy_strimzi
        deploy_kafka
        deploy_es_operator
        deploy_es
        exit 0
        ;;
    "undeploy")
        undeploy_es
        undeploy_es_operator
        undeploy_kafka
        undeploy_strimzi
        undeploy_grafana
        undeploy_assets
        exit 0
        ;;
    "cicd")
        deploy_assets
        exit 0
        ;;
    "uncicd")
        undeploy_assets
        exit 0
        ;;
    "grafana")
        deploy_grafana
        exit 0
        ;;
    "ungrafana")
        undeploy_grafana
        exit 0
        ;;
    "kafka")
        deploy_strimzi
        deploy_kafka
        exit 0
        ;;
    "unkafka")
        undeploy_kafka
        undeploy_strimzi
        exit 0
        ;;
    "es")
        deploy_es_operator
        deploy_es
        exit 0
        ;;
    "unes")
        undeploy_es
        undeploy_es_operator
        exit 0
        ;;
    "start_k8s")
        run_minikube
        ;;
    "stop_k8s")
        minikube stop -p ${MINIKUBE_PROFILE}
        ;;
    "rm_k8s")
        clean_minikube
        ;;
    *)
        die "Usage: $0 (all|undeploy|cide|uncicd|kafka|unkafka|es|unes|start_k8s|rm_k8s)"
        ;;
esac
