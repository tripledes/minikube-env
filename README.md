# CICD Stack

## Stack

* Persistent storage:
  * Jenkins, 2 volumes, 1 for JENKINS_HOME and another for the installed plugins
  * Gogs, 1 volume to store data, repositories, ...
  * Kafka and Zookeeper both use persistent storage for storing their states
* Jenkins (Port 8080 exposed on port 30080)
* Gogs (Port 3000 exposed on port 30300)
* SonarQube (Port 9000 exposed on port 30900)
* Kafka
  * Bootstrap exposed on port 32100
  * Broker 0 exposed on port 32000
* ElasticSearch
  * Exposed on port 30920


## Run it

### Requirements

* [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
* [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [kubens](https://github.com/ahmetb/kubectx)

### Deployment

* Start/Stop/Delete Minikube

```console
$ ./deploy.sh start_k8s
[INFO] ==> Starting minikube profile tfm ...
...
$ ./deploy.sh stop_k8s
✋  Stopping "tfm" in docker ...
...
$ ./deploy.sh rm_k8s
✋  Stopping "tfm" in docker ...
...
```

* Everything

```console
$ ./deploy.sh all
[INFO] ==> applying asset ./kubernetes/assets/00_cicdns.yaml ...
[INFO] ==> applying asset ./kubernetes/assets/04_jenkins_pvc.yaml ...
...
$ ./deploy.sh undeploy
[INFO] ==> deleting asset ./kubernetes/assets/16_sonarqube_svc.yaml ...
[INFO] ==> deleting asset ./kubernetes/assets/15_sonarqube_deployment.yaml ...
...
```

* Kafka

```console
$ ./deploy.sh kafka
[INFO] ==> Creating strimzi namespaces ..
namespace/strimzi created
[INFO] ==> Deploying strimzi operator ..
customresourcedefinition.apiextensions.k8s.io/kafkas.kafka.strimzi.io created
...
$ ./deploy.sh unkafka
[INFO] ==> Deleting Kafka namespaces ...
namespace "streams" deleted
[INFO] ==> Deploying Strimzi operator ...
warning: deleting cluster-scoped resources, not scoped to the provided namespace
...
```

* ElasticSearch

```console
$ ./deploy.sh es
[INFO] ==> Deploying Elastic operator ...
customresourcedefinition.apiextensions.k8s.io/apmservers.apm.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/elasticsearches.elasticsearch.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/kibanas.kibana.k8s.elastic.co created
...
$ ./deploy.sh unes
[INFO] ==> Undeploying ElasticSearch ...
namespace "elasticsearch" deleted
[INFO] ==> Undeploying Elastic operator ...
warning: deleting cluster-scoped resources, not scoped to the provided namespace
...
```

* CI/CD

```console
$ ./deploy.sh cicd
[INFO] ==> applying asset ./kubernetes/assets/00_cicdns.yaml ...
[INFO] ==> applying asset ./kubernetes/assets/04_jenkins_pvc.yaml ...
...
$ ./deploy.sh uncicd
[INFO] ==> deleting asset ./kubernetes/assets/16_sonarqube_svc.yaml ...
[INFO] ==> deleting asset ./kubernetes/assets/15_sonarqube_deployment.yaml ...
...
```

## Working with the services

* Finding the minikube node IP

  ```console
  $ kubectl get node tfm -o jsonpath='{.status.addresses[?(@.type=="InternalIP")].address}'
  192.168.39.140
  ```

* Find Jenkins admin initial password

  ```console
  $ kubectl exec -it $(kubectl get pods --selector=app=jenkins -n cicdns --output=jsonpath={.items..metadata.name}) -n cicdns -- cat /var/jenkins_home/secrets/initialAdminPassword
  edda97bc134543299df54e7865a9840a
  ```
  
* Connecting to services
  * Jenkins: http://192.168.39.140:30080
  * Gogs: http://192.168.39.140:30300
  * SonarQube: http://192.168.39.140:30900 (admin:admin)
  * Point your client to the bootstrap port (32100) on the Node's IP address.
  * Grafana, http://192.168.39.140:31300 (admin:admin)

* Connecting to Kafka using *kaf*

  * Topics: measure-topic, sensor-topic, short-term-streams, long-term-streams
  * Users: gateway-user, streams-user
  * Producer

    * Message

      ```json
      {
        "Temperature": 22.209919609068038,
        "TakenAt": "2020-05-31T14:59:02Z",
        "Metadata": {
          "ID": "asdfasdf834rasdf",
          "Name": "MONTCADA",
          "Location": {
            "Latitude": 88,
            "Longitude": 281.999
          }
        }
      }
      ```

    ```console
    $ kaf produce measure-topic -b 172.17.0.3:32100 < msg.json
    ```

  * Consumer

    ```console
    $ kaf consume short-term-streams -b 172.17.0.3:32100
    Partition:   0
    Offset:      0
    Timestamp:   2020-06-07 22:54:01.653 +0200 CEST
    {
      "Metadata": {
        "ID": "asdfasdf834rasdf",
        "Location": {
          "Latitude": 88,
          "Longitude": 281.999
        },
        "Name": "MONTCADA"
      },
      "TakenAt": "2020-05-31T14:59:02Z",
      "Temperature": 22.209919609068038
    }
    ```

* Testing ElasticSearch

  ```console
  $ PASSWORD="S1p3Rs3Cr3tTFM"
  $ curl -u "tfm:$PASSWORD" -k https://172.17.0.2:30920
  {
    "name" : "meteo-lt-cluster-es-default-0",
    "cluster_name" : "meteo-lt-cluster",
    "cluster_uuid" : "KufZbBshSzui9pJq40bpgg",
    "version" : {
      "number" : "7.6.0",
      "build_flavor" : "default",
      "build_type" : "docker",
      "build_hash" : "7f634e9f44834fbc12724506cc1da681b0c3b1e3",
      "build_date" : "2020-02-06T00:09:00.449973Z",
      "build_snapshot" : false,
      "lucene_version" : "8.4.0",
      "minimum_wire_compatibility_version" : "6.8.0",
      "minimum_index_compatibility_version" : "6.0.0-beta1"
    },
    "tagline" : "You Know, for Search"
  }
  ```
